#!/bin/sh

status=$((0)) 

dfx canister call tzService put \
    '(record {timezoneId="Africa/Abidjan";
    countryCode="CI";
    dstOffset=0.0;
    rawOffset=0.0;
    gmtOffset=0.0})' | grep ok
status=$((status + $?))

dfx canister call tzService put \
    '(record {timezoneId="Africa";
    countryCode="CI";
    dstOffset=0.0;
    rawOffset=0.0;
    gmtOffset=0.0})' | grep 'Invalid timezoneId'
status=$((status + $?))

dfx canister call tzService put \
    '(record {timezoneId="Africa/Abidjan";
    countryCode="C";
    dstOffset=0.0;
    rawOffset=0.0;
    gmtOffset=0.0})' | grep 'Invalid countryCode'
status=$((status + $?))

dfx canister call tzService put \
    '(record {timezoneId="Africa/Abidjan";
    countryCode="CIII";
    dstOffset=0.0;
    rawOffset=0.0;
    gmtOffset=0.0})' | grep 'Invalid countryCode'
status=$((status + $?))

dfx canister call tzService put \
    '(record {timezoneId="Africa/Abidjan";
    countryCode="CI";
    dstOffset=-20.0;
    rawOffset=0.0;
    gmtOffset=0.0})' | grep 'Invalid dstOffset'
status=$((status + $?))

dfx canister call tzService put \
    '(record {timezoneId="Africa/Abidjan";
    countryCode="CI";
    dstOffset=20.0;
    rawOffset=0.0;
    gmtOffset=0.0})' | grep 'Invalid dstOffset'
status=$((status + $?))

dfx canister call tzService put \
    '(record {timezoneId="Africa/Abidjan";
    countryCode="CI";
    dstOffset=0.0;
    rawOffset=-21.0;
    gmtOffset=0.0})' | grep 'Invalid rawOffset'
status=$((status + $?))

dfx canister call tzService put \
    '(record {timezoneId="Africa/Abidjan";
    countryCode="CI";
    dstOffset=0.0;
    rawOffset=21.0;
    gmtOffset=0.0})' | grep 'Invalid rawOffset'
status=$((status + $?))

dfx canister call tzService put \
    '(record {timezoneId="Africa/Abidjan";
    countryCode="CI";
    dstOffset=0.0;
    rawOffset=0.0;
    gmtOffset=-20.0})' | grep 'Invalid gmtOffset'
status=$((status + $?))

dfx canister call tzService put \
    '(record {timezoneId="Africa/Abidjan";
    countryCode="CI";
    dstOffset=0.0;
    rawOffset=0.0;
    gmtOffset=20.0})' | grep 'Invalid gmtOffset'
status=$((status + $?))

exit $((status + $?))