#!/bin/sh

status=$((0)) 

dfx canister call tzService findByIdPath '("Europe")' | grep '"Europe/Vatican"'

status=$((status + $?))

dfx canister call tzService findByIdPath '("America/Argentina")' | grep '"America/Argentina/Mendoza"'

exit $((status + $?))