#!/bin/sh

status=$((0)) 

dfx canister call tzService getAll | grep '{ timezoneId = "Asia/Yakutsk"; countryCode = "RU"; dstOffset = 9.0 : float64; rawOffset = 9.0 : float64; gmtOffset = 9.0 : float64;}'

exit $((status + $?))