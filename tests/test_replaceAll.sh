#!/bin/sh

status=$((0)) 

DATA=$(cat << EOF
(vec{
record {timezoneId="Africa/Abidjan"; countryCode="CI"; dstOffset=0.0; rawOffset=0.0; gmtOffset=0.0};
record {timezoneId="Asia/Ho_Chi_Minh"; countryCode="VN"; dstOffset=7.0; rawOffset=7.0; gmtOffset=7.0};
record {timezoneId="Europe/Gibraltar"; countryCode="GI"; dstOffset=1.0; rawOffset=2.0; gmtOffset=1.0};
})
EOF
);

dfx canister call tzService replaceAll "$DATA" | grep ok

status=$((status + $?))

DATA=$(cat << EOF
(vec{
record {timezoneId="Africa/Abidjan"; countryCode="CI"; dstOffset=0.0; rawOffset=0.0; gmtOffset=0.0};
record {timezoneId="Asia/Ho_Chi_Minh"; countryCode="VN"; dstOffset=7.0; rawOffset=7.0; gmtOffset=7.0};
record {timezoneId="Europe/Gibraltar"; countryCode="GI"; dstOffset=1.0; rawOffset=2.0; gmtOffset=100.0};
})
EOF
);

dfx canister call tzService replaceAll "$DATA" | grep 'Invalid'

exit $((status + $?))