#!/bin/sh

status=$((0)) 

dfx canister call tzService setIsJsonApiActivated '(false)' | grep '(variant { ok })'

status=$((status + $?))

wget -q http://r7inp-6aaaa-aaaaa-aaabq-cai.localhost:8000/json -O /dev/null

status=$((status + ($?!=8)))

dfx canister call tzService setIsJsonApiActivated '(true)' | grep '(variant { ok })'

status=$((status + $?))

dfx canister call tzService getIsJsonApiActivated | grep '(variant { ok = true })'

status=$((status + $?))

wget -q http://r7inp-6aaaa-aaaaa-aaabq-cai.localhost:8000/json -O /dev/null

status=$((status + $?))

exit $((status + $?))