#!/bin/sh

status=$((0)) 

dfx canister call tzService setTimezoneLimit '(8)' | grep ok

status=$((status + $?))

dfx canister call tzService getTimezoneLimit | grep '(8 : nat)'

exit $((status + $?))