#!/bin/sh

status=$((0)) 

dfx canister call tzService setGmtOffsetLimits '(-12.0,14.0)' | grep ok

status=$((status + $?))

dfx canister call tzService getGmtOffsetLimits | grep '(-12.0 : float64, 14.0 : float64)'

exit $((status + $?))