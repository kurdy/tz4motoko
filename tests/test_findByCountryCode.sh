#!/bin/sh

status=$((0)) 

dfx canister call tzService findByCountryCode '("FR")' | grep '"Europe/Paris"'

status=$((status + $?))

dfx canister call tzService findByCountryCode '("CL")' | grep '"America/Punta_Arenas"'

exit $((status + $?))