#!/bin/sh

status=$((0)) 

dfx canister call tzService setCountryCodeSizeLimits '(2,3)' | grep ok

status=$((status + $?))

dfx canister call tzService getCountryCodeSizeLimits | grep '(2 : nat, 3 : nat)'

exit $((status + $?))