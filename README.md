# Tz4Motoko

Motoko Timezone service for Internet Computer. 

Tz4Motoko provides timezone data in two ways:

1. As a reusable canister in a project using the Internet Computer Ecosystem.

Sample of the configuration in dfx.json
```JSON
"tzService": {
    "main": "src/tzService/main.mo",
    "type": "motoko"
}
```  

2. A JSON API service deployable on the Internet Computer network

## Data source 

The data is retrieved from the site [GeoNames - WebServices](https://www.geonames.org/export/web-services.html#timezone)

GeoNames data is licensed under the [Creative Commons Attribution 4.0 License](https://creativecommons.org/licenses/by/4.0/)

## Wiki

[Home](https://gitlab.com/kurdy/tz4motoko/-/wikis/Timezone-for-Motoko)

[API](https://gitlab.com/kurdy/tz4motoko/-/wikis/API)

### Example of the timezone object

```JSON
{
    "timezoneId" : "Europe/Vatican",
    "countryCode" : "VA",
    "gmtOffset" : 1.000000,
    "dstOffset" : 2.000000,
    "rawOffset" : 1.000000
}
```

## Local

* UI [tzServive](http://127.0.0.1:8000/?canisterId=rkp4c-7iaaa-aaaaa-aaaca-cai&id=r7inp-6aaaa-aaaaa-aaabq-cai)
* Web [Web](http://127.0.0.1:8000/?canisterId=ryjl3-tyaaa-aaaaa-aaaba-cai#/)
* API JSON
    * [All timezone objects](http://r7inp-6aaaa-aaaaa-aaabq-cai.localhost:8000/json)
    * [Country code list](http://r7inp-6aaaa-aaaaa-aaabq-cai.localhost:8000/json/country_code_list) → `["CX","MN",...,"CW"]`
    * [Zone list](http://r7inp-6aaaa-aaaaa-aaabq-cai.localhost:8000/json/zone_list) → `["Pacific","Indian","Asia","America","Antarctica","Africa","Europe","Australia","Atlantic","Arctic"]`
    * [country_code=US](http://r7inp-6aaaa-aaaaa-aaabq-cai.localhost:8000/json?country_code=US)
    * [path=Europe](http://r7inp-6aaaa-aaaaa-aaabq-cai.localhost:8000/json?path=Europe)
    * [path=America/Argentina](http://r7inp-6aaaa-aaaaa-aaabq-cai.localhost:8000/json?path=America/Argentina)

### Build & Dev

#### Prerequisites

* [dFinity SDK](https://smartcontracts.org) or 
    * `sh -ci "$(curl -fsSL https://smartcontracts.org/install.sh)"`
* npm

#### Clone repo. 

`git clone https://gitlab.com/kurdy/tz4motoko.git`

`cd tz4motoko`

#### Install dependencies

`npm install`

#### Starts the local replica and a web server 

`dfx start &`

#### Deploy

`dfx deploy`

### Tests

```sh
sh ./tests/test_DstOffsetLimits.sh;
sh ./tests/test_GmtOffsetLimits.sh;
sh ./tests/test_RawOffsetLimits.sh;
sh ./tests/test_CountryCodeSizeLimits.sh;
sh ./tests/test_TimezoneLimit.sh;
sh ./tests/test_put.sh;
sh ./tests/test_replaceAll.sh;
sh ./tests/test_replaceAll2.sh;
sh ./tests/test_findByCountryCode.sh
sh ./tests/test_findByIdPath.sh
sh ./tests/test_getAll.sh
sh ./tests/test_getCountryCodeList.sh
sh ./tests/test_getZoneList.sh
sh ./tests/test_JsonApi.sh
```

### update data

[Default data source https://download.geonames.org/export/dump/timeZones.txt ](https://download.geonames.org/export/dump/timeZones.txt)

`sh scripts/update_tz.sh`

# Online

* WEB UI
    * [4tpm5-jaaaa-aaaal-aahma-cai.icp0.io](https://4tpm5-jaaaa-aaaal-aahma-cai.icp0.io/)
        * [UpTimeRobot](https://stats.uptimerobot.com/KGgPAsD4wy/790396891)
    * [tz.kurdy.org](https://tz.kurdy.org)
* [API JSON: getAll](https://4uokj-eyaaa-aaaal-aahmq-cai.raw.icp0.io/json)
* [API JSON: country_code_list](https://4uokj-eyaaa-aaaal-aahmq-cai.raw.icp0.io/json/country_code_list)
* [API JSON: zone_list](https://4uokj-eyaaa-aaaal-aahmq-cai.raw.icp0.io/json/zone_list)
* [API JSON: country_code=US](https://4uokj-eyaaa-aaaal-aahmq-cai.raw.icp0.io/json?country_code=US)
* [API JSON: path=Europe](https://4uokj-eyaaa-aaaal-aahmq-cai.raw.icp0.io/json?path=Europe)
* [API JSON: path=America/Argentina](https://4uokj-eyaaa-aaaal-aahmq-cai.raw.icp0.io/json?path=America/Argentina)
* [API JSON: path=America/Argentina/Cordoba](https://4uokj-eyaaa-aaaal-aahmq-cai.raw.icp0.io/json?path=America/Argentina/Cordoba)
