/// Implementation of Timezones, this service manages timezone based on GeoNames data. 
/// It also exposes some methods in the form of a get API with a JSON response 
/// Licence BSD-3-Clause
/// https://gitlab.com/kurdy/tz4motoko

import Float "mo:base/Float";
import Nat "mo:base/Nat";
import Text "mo:base/Text";
import Result "mo:base/Result";
import HashMap "mo:base/HashMap";
import Iter "mo:base/Iter";
import Debug "mo:base/Debug";
import Hash "mo:base/Hash";
import Principal "mo:base/Principal";
import Array "mo:base/Array";
import Char "mo:base/Char";
import TrieSet "mo:base/TrieSet";
import Option "mo:base/Option";
import Bool "mo:base/Bool";
import Time "mo:base/Time";
import Int "mo:base/Int";
import Buffer "mo:base/Buffer";

/// Timezones canister
shared({caller = canisterOwner}) actor class TimeZones() {

    type Result<T,E> = Result.Result<T,E>;

    type Time = Time.Time;

    /// Timezone type
    public type TimeZone = {
        timezoneId : Text;
        countryCode : Text;
        gmtOffset : Float;
        dstOffset : Float;
        rawOffset : Float;
    };

    // Validation limits
    stable var minTimezoneIdSize : Nat = 8;

    stable var minCountryCodeSize : Nat = 2;
    stable var maxCountryCodeSize : Nat = 3;

    stable var minGmtOffset : Float = -12.0;
    stable var maxGmtOffset : Float = 14.0;

    stable var minDstOffset : Float = -12.0;
    stable var maxDstOffset : Float = 14.0;

    stable var minRawOffset : Float = -12.0;
    stable var maxRawOffset : Float = 14.0;

    // Need to be true to enable API JSON
    stable var isJsonApiActivated : Bool = false;

    // Timestamp of last data refresh
    var lastDataUpdate : Time = Time.now();

    // Data store used during upgrade
    stable var timezoneStore : [(Text, TimeZone)] = [];
    var timezoneSize : Nat = 400;

    // Current timezones
    var timezoneMap = HashMap.fromIter<Text, TimeZone>(
        timezoneStore.vals(),timezoneSize,func (x, y) { Text.equal(x, y) }, func(x){ Text.hash(x)});
    

    system func preupgrade() {
        Debug.print ("Call of TimeZones:preupgrade.");
        timezoneStore := Iter.toArray(timezoneMap.entries());
        timezoneSize := Iter.size<(Text, TimeZone)>(timezoneStore.vals());
    };

    system func postupgrade() {
        Debug.print ("Call of TimeZones:postupgrade.");
        timezoneStore := [];
        timezoneSize := 400;
    };

    /// Put a new timezone object.
    /// Only owner of canister can do it
    public shared({caller}) func put(tz : TimeZone) : async Result<(),Text>  {
        if (Principal.equal(canisterOwner,caller)) {
            let isValid = isTimezoneValid(tz);
            switch(isValid) {
                case (#ok()) {
                    lastDataUpdate := Time.now();
                    timezoneMap.put(tz.timezoneId,tz);
                    return #ok();
                };
                case (#err(msg)) return #err(msg);
            };
        } else {
            return #err("Caller isn't allowed to modify data's. Caller is : " # Principal.toText(caller));
        };
    };

    /// Replace all timezone
    /// Only owner of canister can do it
    public shared({caller}) func replaceAll(allTz : [TimeZone]) : async Result<(),[Text]>  {
        let size : Nat = Iter.size(Array.vals(allTz));
        var newData = Buffer.Buffer<(Text, TimeZone)>(size);
        var errors = Buffer.Buffer<Text>(0); 
        if (Principal.equal(canisterOwner,caller)) {
            lastDataUpdate := Time.now();
            for (tz in Array.vals(allTz)) {
                var isValid = isTimezoneValid(tz);
                switch(isValid) {
                    case (#ok()) newData.add((tz.timezoneId,tz));
                    case (#err(msg)) errors.add(msg);
                }
            };
        } else {
            errors.add("Caller isn't allowed to modify data's. Caller is : " # Principal.toText(caller));//Array.append<Text>(errors,Array.make<Text>(("Caller isn't allowed to modify data's. Caller is : " # Principal.toText(caller))));
        };
        if (errors.size() == 0) {
            timezoneMap := HashMap.fromIter<Text, TimeZone>(
                newData.vals(),size,func (x, y) { Text.equal(x, y) }, func(x){Text.hash(x)});
            #ok();
        } else {
            #err(Buffer.toArray<Text>(errors));
        };
    };

    /// Get all timezones as an array
    public query func getAll() : async [TimeZone] {
        return Iter.toArray<TimeZone>(timezoneMap.vals());
    };

    /// Find by Id
    /// id can be America/Santiago
    public query func findById(id : Text) : async ?TimeZone {
        return timezoneMap.get(id);
    };

    /// Find by path
    /// Path can be America or America/Argentina
    public query func findByIdPath(path : Text) : async [TimeZone] {
        return _findByIdPath(path);
    };

    /// Find by country code
    /// Code can be FR
    public query func findByCountryCode(code : Text) : async [TimeZone] {
        return _findByCountryCode(code)
    };

    private func _findByIdPath(path : Text) : [TimeZone] {
        let zones = HashMap.mapFilter<Text,TimeZone,TimeZone>(timezoneMap,Text.equal,Text.hash,func(k : Text,v : TimeZone): ?TimeZone{ if(Text.startsWith(k, #text path)) ?v else null});
        return Iter.toArray<TimeZone>(zones.vals());
    };

    private func _findByCountryCode(code : Text) : [TimeZone] {
        let zones = HashMap.mapFilter<Text,TimeZone,TimeZone>(timezoneMap,Text.equal,Text.hash,func(k : Text,v : TimeZone): ?TimeZone{ if(Text.equal(v.countryCode, code)) ?v else null});
        return Iter.toArray<TimeZone>(zones.vals());
    };

    /// get an array of all country code
    public query func getCountryCodeList() : async [Text] {
        return _getCountryCodeList();
    };

    private func _getCountryCodeList() : [Text] {
        let data : [Text] = Array.map<TimeZone,Text>(Iter.toArray<TimeZone>(timezoneMap.vals()),func(x){x.countryCode});
        return TrieSet.toArray<Text>(TrieSet.fromArray<Text>(data,func (x){Text.hash(x)},func (x, y){Text.equal(x, y)}));   
    };

    /// Get an array of all first level zones
    /// ["Pacific","Indian","Asia","America","Antarctica","Africa","Europe","Australia","Atlantic","Arctic"]
    public query func getZoneList() : async [Text] {
        return _getZoneList();
    };

    private func _getZoneList() : [Text] {
        let data : [Text] = Array.map<TimeZone,Text>(Iter.toArray<TimeZone>(timezoneMap.vals()),func(x){ Option.get(Text.tokens(x.timezoneId,#text "/").next(),"")});
        return TrieSet.toArray<Text>(TrieSet.fromArray<Text>(data,func (x){Text.hash(x)},func (x, y){Text.equal(x, y)}));   
    };

    /// Enable or disable API JSON
    /// True = Enabled
    public shared({caller}) func setIsJsonApiActivated(isActive : Bool ) : async Result<(),Text> {
        if (Principal.equal(canisterOwner,caller)) {
            isJsonApiActivated := isActive;
            #ok;
        } else {
            #err("Caller isn't allowed to modify isJsonApiActivated. Caller is : " # Principal.toText(caller));
        };
    };

    /// Get API state enabled/disabled 
    public shared({caller}) func getIsJsonApiActivated( ) : async Result<Bool,Text> {
        if (Principal.equal(canisterOwner,caller)) {
            #ok(isJsonApiActivated);
        } else {
            #err("Caller isn't allowed to get isJsonApiActivated. Caller is : " # Principal.toText(caller));
        };
    };

    // Validate time zone object
    private func isTimezoneValid(tz : TimeZone) : Result<(),Text> {

        if (Nat.lessOrEqual(Text.size(tz.timezoneId),minTimezoneIdSize)) {
            return #err("Invalid timezoneId " # tz.timezoneId);
        };

        if (Nat.less(Text.size(tz.countryCode),minCountryCodeSize)) {
            return #err("Invalid countryCode " # tz.countryCode);
        };

        if (Nat.greater(Text.size(tz.countryCode),maxCountryCodeSize)) {
            return #err("Invalid countryCode " # tz.countryCode);
        };

        if (Float.less(tz.gmtOffset,minGmtOffset)) {
            return #err("Invalid gmtOffset " # Float.toText(tz.gmtOffset));
        };

        if (Float.greater(tz.gmtOffset,maxGmtOffset)) {
            return #err("Invalid gmtOffset " # Float.toText(tz.gmtOffset));
        };

        if (Float.less(tz.dstOffset,minDstOffset)) {
            return #err("Invalid dstOffset " # Float.toText(tz.dstOffset));
        };

        if (Float.greater(tz.dstOffset,maxDstOffset)) {
            return #err("Invalid dstOffset " # Float.toText(tz.dstOffset));
        };

        if (Float.less(tz.rawOffset,minRawOffset)) {
            return #err("Invalid rawOffset " # Float.toText(tz.rawOffset));
        };

        if (Float.greater(tz.rawOffset,maxRawOffset)) {
            return #err("Invalid rawOffset " # Float.toText(tz.rawOffset));
        };

        return #ok();
    };

    /// Set min length of timezone 
    /// Reserved to owner
    public shared({caller}) func setTimezoneLimit(minSize : Nat) : async Result<(),Text> {
        if (Principal.equal(canisterOwner,caller)) {
            minTimezoneIdSize := minSize;
            #ok();
        } else {
            #err("Caller isn't allowed to modify data's. Caller is : " # Principal.toText(caller));
        };  
        
    };

    /// Get min length of timezone 
    /// Reserved to owner
    public query func getTimezoneLimit () : async Nat {
        return minTimezoneIdSize;
    };

    /// Set min/max length of country code 
    /// Reserved to owner
    public shared({caller}) func setCountryCodeSizeLimits(min: Nat, max : Nat) : async Result<(),Text> {
        if (Principal.equal(canisterOwner,caller)) {
            minCountryCodeSize := min;
            maxCountryCodeSize := max;
            #ok();
        } else {
            #err("Caller isn't allowed to modify data's. Caller is : " # Principal.toText(caller));
        };    
    };

    /// Get min/max length of country code 
    /// Reserved to owner
    public query func getCountryCodeSizeLimits() : async (Nat,Nat) {
        return (minCountryCodeSize, maxCountryCodeSize);
    };

    /// Set min/max values of Gmt Offset
    /// Reserved to owner
    public shared({caller}) func setGmtOffsetLimits(min: Float, max : Float) : async Result<(),Text> {
        if (Principal.equal(canisterOwner,caller)) {
            minGmtOffset := min;
            maxGmtOffset := max;
            #ok();
        } else {
            #err("Caller isn't allowed to modify data's. Caller is : " # Principal.toText(caller));
        };  
    };

    /// Get min/max values of Gmt Offset
    /// Reserved to owner
    public query func getGmtOffsetLimits() : async (Float,Float) {
        return (minGmtOffset, maxGmtOffset);
    };

    /// Set min/max values of DST Offset
    /// Reserved to owner
    public shared({caller}) func setDstOffsetLimits(min: Float, max : Float) : async Result<(),Text> {
        if (Principal.equal(canisterOwner,caller)) {
            minDstOffset := min;
            maxDstOffset := max;
            #ok();
        } else {
            #err("Caller isn't allowed to modify data's. Caller is : " # Principal.toText(caller));
        };
        
    };

    /// Get min/max values of DST Offset
    /// Reserved to owner
    public query func getDstOffsetLimits() : async (Float,Float) {
        return (minDstOffset, maxDstOffset);
    };

    /// Set min/max values of Raw Offset
    /// Reserved to owner
    public shared({caller}) func setRawOffsetLimits(min: Float, max : Float) : async Result<(),Text> {
        if (Principal.equal(canisterOwner,caller)) {
            minRawOffset := min;
            maxRawOffset := max;
            #ok();
        } else {
            #err("Caller isn't allowed to modify data's. Caller is : " # Principal.toText(caller));
        };
    };

    /// Get min/max values of Raw Offset
    /// Reserved to owner
    public query func getRawOffsetLimits() : async (Float,Float) {
        return (minRawOffset, maxRawOffset);
    };

    /// Get number of time zone stored
    public query func getNumberOfTimezone() : async Nat {
        timezoneMap.size();
    };

    /// Remove / clear store
    /// Reserved to owner
    public shared({caller}) func removeAll() : async Result<(),Text> {
        if (Principal.equal(canisterOwner,caller)) {
            timezoneMap := HashMap.HashMap<Text, TimeZone>(400, Text.equal, Text.hash);
            #ok();
        } else {
            #err("Caller isn't allowed to remove all timezones. Caller is : " # Principal.toText(caller));
        };  
        
    };

    /***
        API Part
    ***/

    // Basic http request type
    type HttpRequest = {
        method : Text;
        url : Text;
        headers : [HeaderField];
        body : Blob;
    };

    // Basic http response type
    type HttpResponse = {
        status_code : Nat16;
        headers : [HeaderField];
        body : Blob;
    };

    // Header field
    type HeaderField = (Text, Text);

    /// API HTTP Request
    public query func http_request(request : HttpRequest) : async HttpResponse {
        assert isJsonApiActivated; // Deliberately brutal as trap
        Debug.print ("HttpRequest: method=" # request.method # " url=" # request.url);
        var content : Text = "";
        var code : Nat16 = 200;
        
        // Select request url and extract parameter if any
        if (Text.startsWith(request.url, #text "/json?country_code=")) {
            let items = Text.tokens(request.url, #text "=");
            let not_used=items.next();
            switch(items.next()) {
                case (?c) content := toJson(_findByCountryCode(c));
                case _ {
                    code := 404;
                    content := "{\"message\" : \"Not found country_code value\"}";
                };
            };
        } else if (Text.startsWith(request.url, #text "/json?path=")) {
            let items = Text.tokens(request.url, #text "=");
            let not_used=items.next();
            switch(items.next()) {
                case (?p) content := toJson(_findByIdPath(p));
                case _ {
                    code := 404;
                    content := "{\"message\" : \"Not found path value\"}";
                };
            };
        } else if (Text.equal(request.url, "/json/country_code_list")) {
            content := textArrayToJson(_getCountryCodeList());
        } else if (Text.equal(request.url, "/json/zone_list")) {
            content := textArrayToJson(_getZoneList());
        } else if (Text.equal(request.url, "/json")) {
            content := toJson(Iter.toArray(timezoneMap.vals()));
        } else {
            code := 404;
            content := "{\"message\" : \"Not supported\"}";
        };

        // Build response
        return {
            status_code = code;
            headers = [("content-type", "text/json; charset=utf-8"),("Content-Length", Nat.toText(content.size())),("Cache-Control", "Public, ETag=" # Int.toText(lastDataUpdate))];
            body = Text.encodeUtf8(content);
        };
    };

    // Convert array of TimeZone to JSON
    private func toJson(timezones : [TimeZone]) : Text {
        var items : Text = "";
        for (tz in Array.vals(timezones)){
            if(Text.size(items) != 0) {
                items := Text.concat(items,",");
            };
            items := Text.concat(items,timeZoneToJson(tz));
        };
        return "[" # items # "]";       
    };

    // Convert TimeZone to JSON
    private func timeZoneToJson(tz : TimeZone) : Text {
        let obj : Text = "{"
        # "\"timezoneId\""
        # " : "
        # Char.toText('\"')
        # tz.timezoneId
        # Char.toText('\"')
        # ","
        # "\"countryCode\""
        # " : "
        # Char.toText('\"')
        # tz.countryCode
        # Char.toText('\"')
        # ","
        # "\"gmtOffset\""
        # " : "
        # Float.toText(tz.gmtOffset)
        # ","
        # "\"dstOffset\""
        # " : "
        # Float.toText(tz.dstOffset)
        # ","
        # "\"rawOffset\""
        # " : "
        # Float.toText(tz.rawOffset)
        # "}";
        return obj;
    };

    // Convert Text array to JSON
    private func textArrayToJson(ar : [Text]) : Text {
          var items : Text = "";
          for (t in Array.vals(ar)){
            if(Text.size(items) != 0) {
                items := Text.concat(items,",");
            };
            items := Text.concat(items,"\"" # t # "\"");
          };
          return "[" # items # "]";    
     };

};