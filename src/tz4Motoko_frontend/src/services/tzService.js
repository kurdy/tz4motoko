import { tzService as canister } from "../../../declarations/tzService";

export async function getAll() {
    let tz = [];
    await canister.getAll()
        .then((data)=>{
            tz = [...data];
        }).catch((reason)=>{
            console.warn("Couldn't getAll() from tzService: " + reason);
        });
    return tz;
};

export async function getCountryCodeList() {
    let cc = [];
    await canister.getCountryCodeList()
        .then((data)=>{
            cc = [...data];
        }).catch((reason)=>{
            console.warn("Couldn't getCountryCodeList() from tzService: " + reason);
        });
    return cc;
};

export async function getZoneList() {
    let zones = [];
    await canister.getZoneList()
        .then((data)=>{
            zones = [...data];
        }).catch((reason)=>{
            console.warn("Couldn't getZoneList() from tzService: " + reason);
        });
    return zones;
};