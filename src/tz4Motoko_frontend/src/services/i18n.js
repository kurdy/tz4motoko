import fr from '../i18n/fr.json';
import en from '../i18n/en.json';
import de from '../i18n/de.json';
import {locale as primeLocale,addLocale as primeAddLocale,updateLocaleOption,localeOption} from 'primereact/api';

//const supported_locale = ['fr','en','de'];

export async function getMessage() {
    switch (getLocale()) {
        case 'fr' : {
            primeAddLocale('fr',fr.primeI18n)
            primeLocale('fr');
            updateLocaleOption('dateFormat',indentifyShortDateFormat(),'fr');
            console.debug('dateFormat:' + localeOption('dateFormat','fr'));
            return fr.reactIntl
        };
        case 'de' : {
            primeAddLocale('de',fr.primeI18n)
            primeLocale('de');
            updateLocaleOption('dateFormat',indentifyShortDateFormat(),'de');
            console.debug('dateFormat:' + localeOption('dateFormat','de'));
            return de.reactIntl
        };
        default : {
            updateLocaleOption('dateFormat',indentifyShortDateFormat(),'en');
            console.debug('dateFormat:' + localeOption('dateFormat','en'));
            return en
        };
    };
};

export function getLocale() {
    const locale = localStorage.getItem("app.locale") != null ? localStorage.getItem("app.locale") : navigator.language;
    switch (locale.substring(0,2)) {
        case 'fr' : return 'fr';
        case 'de' : return 'de';
        default : return 'en';
    };
};

export function getDefaultLocale () {
    return 'en';
};

export async function setLocale (locale) {
    localStorage.setItem("app.locale",locale);
};

function indentifyShortDateFormat(){
    const options = { month: "numeric", day: "numeric", year: "numeric" };
    const date = Date.parse("1985-11-23T22:22:22.222Z");
    let formatedDate = new Intl.DateTimeFormat(navigator.language, options).format(date);
    formatedDate=formatedDate.replace("1985","yy");
    formatedDate=formatedDate.replace("11","mm");
    formatedDate=formatedDate.replace("23","dd");
    return formatedDate;
};