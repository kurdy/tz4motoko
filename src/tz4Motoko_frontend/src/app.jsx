import React, { useEffect,useState} from 'react';
import { IntlProvider } from "react-intl";
import Home from "./components/home";
import {getDefaultLocale,getLocale,getMessage} from "./services/i18n.js";

import { HashRouter as Router, Route, Routes } from 'react-router-dom';

import PrimeReact from 'primereact/api';

import { ProgressSpinner } from 'primereact/progressspinner';

import 'primereact/resources/themes/lara-light-teal/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';

// Ripple is an optional animation for the supported components such as buttons.
PrimeReact.ripple = true;

// Input fields come in two styles, default is outlined with borders around the field whereas filled
PrimeReact.inputStyle = 'filled';

import "@fontsource/poppins";

function App() {
  const [locale,setLocale] = useState(getLocale());
  const [messages,setMessages] = useState(undefined);

  useEffect(() => {
    console.info('mount App');
    getMessage().then((m)=>setMessages(m));
  }, [])

  const fallback = <div className="flex justify-content-start align-items-center min-h-screen"><ProgressSpinner/></div>;

  const Display = () => {
    if (messages!=undefined) {
      return (
        <IntlProvider messages={messages} locale={locale} defaultLocale={getDefaultLocale()}>
            <div className="flex flex-column min-h-screen bg-primary-reverse">
              <Router>
                <div className="flex flex-grow-1">
                    <Routes>
                      <Route exact path="/" element={<Home/>}/>
                    </Routes>
                </div>
              </Router> 
            </div>   
        </IntlProvider>
      );
    } else {
      return fallback
    }
  }

  return <Display/>
}

export default App;