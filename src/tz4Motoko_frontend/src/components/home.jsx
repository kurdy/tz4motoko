/*
* Part of project tz4Motoko
* Home component that represents the content using DataTable.
* BSD 3-Clause License 
* Git repository : https://gitlab.com/kurdy/tz4motoko
*/
import React,{useState, useRef, Fragment,useEffect} from "react";

import {useIntl, FormattedMessage} from 'react-intl';

import { Toolbar } from 'primereact/toolbar';
import { Button } from 'primereact/button';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { FilterMatchMode } from 'primereact/api';
import { InputText } from 'primereact/inputtext';
import { MultiSelect } from 'primereact/multiselect';
import { Dropdown } from 'primereact/dropdown';

import Licence from "./licence";
import Logo from "../images/logo";
import GitLab from "../images/gitlab";

import {getAll,getCountryCodeList,getZoneList} from "../services/tzService"


function Home() {
    const [tzData, setTzData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [filters, setFilters] = useState({
        'global': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'countryCode': { value: null, matchMode: FilterMatchMode.IN },
        'timezoneId': { value: null, matchMode: FilterMatchMode.STARTS_WITH }
    });
    const [countryCodes, setCountryCodes] = useState([]);
    const [zones, setZones] = useState([]);

    const intl = useIntl();
    const dt = useRef(null);

    useEffect(() => {
        getAll().then(data => {setTzData(data);setLoading(false)});
        getCountryCodeList().then(data => setCountryCodes(data.sort()));
        getZoneList().then(data => setZones(data.sort()));
    }, []);

    const paginatorLeft = <Button type="button" icon="pi pi-download" className="p-button-text" disabled={!Array.isArray(tzData) || tzData.length===0} onClick={(e)=>{e.preventDefault();dt.current.exportCSV(false);}} tooltip={intl.formatMessage({ id: 'key.download',defaultMessage: 'Download as csv'})} tooltipOptions={{position: 'left'}}/>;
    const paginatorRight = <Fragment/>;

    const leftContents = (
        <Fragment>
            <Logo size={'3.5rem'} className="mr-3"/>
            <p className="text-4xl text-left font-bold m-0">Timezone</p>
        </Fragment>
    );

    const rightContents = (
        <Fragment>
            <Button className="p-button-rounded p-button-text" onClick={(e)=>{ e.preventDefault();window.open('https://gitlab.com/kurdy/tz4motoko','_blank');}} tooltip={intl.formatMessage({ id: 'key.source',defaultMessage: 'Source code'})} tooltipOptions={{position: 'left'}}>
                <GitLab size={'3.5rem'} className="mr-3"/>
            </Button>
        </Fragment>
    );


    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;

        setFilters(_filters);
        setGlobalFilterValue(value);
    }

    const renderHeader = () => {
        return (
            <div className="flex justify-content-end">
                <span className="p-input-icon-left">
                    <i className="pi pi-search" />
                    <InputText value={globalFilterValue} onChange={onGlobalFilterChange} placeholder={intl.formatMessage({ id: 'key.global.filter.placeholder',defaultMessage: 'Keyword Search'})} />
                </span>
            </div>
        )
    }
    const header = renderHeader();

    const countryCodeItemTemplate = (option) => {
        return (
            <span>{option}</span>
        );
    }

    const countryCodeRowFilterTemplate = (options) => {
        return <MultiSelect value={options.value} options={countryCodes} itemTemplate={countryCodeItemTemplate} onChange={(e) => options.filterApplyCallback(e.value)}  placeholder={intl.formatMessage({ id: 'key.country.code.filter.placeholder',defaultMessage: 'Any'})}  className="p-column-filter" maxSelectedLabels={1} />;
    }

    const zoneItemTemplate = (option) => {
        return (
            <span>{option}</span>
        );
    }

    const zoneRowFilterTemplate = (options) => {
        return <Dropdown value={options.value} options={zones} onChange={(e) => options.filterApplyCallback(e.value)} itemTemplate={zoneItemTemplate} placeholder={intl.formatMessage({ id: 'key.timezone.filter.placeholder',defaultMessage: 'Select a zone'})} className="p-column-filter" showClear />;  
    }

    return (
        <div className="flex flex-column min-h-screen min-w-screen">
            <Toolbar left={leftContents} right={rightContents} className="m-1"/>
            <div className="flex flex-grow-1 card m-2">
                <DataTable 
                    className="min-w-full" 
                    dataKey="timezoneId" filters={filters} filterDisplay="row"
                    loading={loading} responsiveLayout="scroll"
                    globalFilterFields={['global','countryCode','timezoneId']}
                    header={header}
                    ref={dt} value={tzData} size="small" paginator responsiveLayout="scroll"
                    paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown"
                    currentPageReportTemplate={intl.formatMessage({ id: 'key.table.current.page.report',defaultMessage: 'Showing {first} to {last} of {totalRecords}'})}
                    rows={10} rowsPerPageOptions={[10,20,50,100]}
                    paginatorLeft={paginatorLeft} paginatorRight={paginatorRight}
                    emptyMessage={intl.formatMessage({ id: 'key.table.emptyMessage',defaultMessage: 'Dataset is empty'})}
                    stripedRows>
                    <Column field="countryCode" header={intl.formatMessage({ id: 'key.table.header.country.code',defaultMessage: 'Country Code'})} sortable filterField="countryCode" showFilterMenu={false} filter filterElement={countryCodeRowFilterTemplate} filterMenuStyle={{ width: '1rem'}} style={{width:'1rem'}}/>
                    <Column field="timezoneId" header={intl.formatMessage({ id: 'key.table.header.timezone.id',defaultMessage: 'ID'})} sortable filterField="timezoneId" showFilterMenu={false} filter filterElement={zoneRowFilterTemplate} filterMenuStyle={{ width: '3rem'}} style={{width:'3rem'}}/>
                    <Column field="gmtOffset" header={intl.formatMessage({ id: 'key.table.header.gmt',defaultMessage: 'GMT offset'})} sortable/>
                    <Column field="dstOffset" header={intl.formatMessage({ id: 'key.table.header.dst',defaultMessage: 'DST offset'})} sortable/>
                    <Column field="rawOffset" header={intl.formatMessage({ id: 'key.table.header.raw',defaultMessage: 'Raw offset'})} sortable/>
                </DataTable>
            </div>
            <div className="flex flex-grow-0 card surface-card m-1">
                <div className="flex flex-row min-w-full">
                    <div className="flex flex-grow-0 ml-10">
                        <Licence/>
                    </div>
                    <div className="flex flex-grow-1 ml-3 mr-3 justify-content-center">
                        <span className="">Version {process.env.PACKAGE_VERSION}</span>
                    </div>
                    <div className="flex flex-grow-0 mr-10">
                        <span><a href="https://gitlab.com/kurdy/tz4motoko/-/wikis/API" target="_blank"><FormattedMessage id="key.api.doc" defaultMessage="Documentation for the use of the API"/></a></span>
                    </div>
                </div>
            </div>
        </div>
        )
}

export default Home