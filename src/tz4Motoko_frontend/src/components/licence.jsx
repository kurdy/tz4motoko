/*
* Part of project tz4Motoko
* Purpose show licence.
* BSD 3-Clause License 
* Git repository : https://gitlab.com/kurdy/tz4motoko
*/
import React,{useRef, Fragment,useEffect} from "react";
import {FormattedMessage} from 'react-intl';
import { OverlayPanel } from 'primereact/overlaypanel';

function Licence() {
    const op = useRef(null);
    const isMounted = useRef(false);

    useEffect(() => {
        if (isMounted.current) {
            op.current.hide();
        }
    }, []);

    return (
        <Fragment>
            <OverlayPanel ref={op} showCloseIcon id="overlay_panel" className="w-6 ml-4">
                <p className="text-xl text-center">BSD 3-Clause License</p>
                <p className="text-base text-center">Copyright (c) 2022, ʕʘ̅͜ʘ̅ʔ</p>
                <p className="text-base text-center mb-6">All rights reserved.</p>
                <p className="text-base text-justify mb-3">Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:</p>
                <p className="text-base text-justify mb-3">1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.</p>
                <p className="text-base text-justify mb-3">2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.</p>
                <p className="text-base text-justify mb-3">3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.</p>
                <p className="text-base text-justify uppercase">THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
            </OverlayPanel>
            <span className="flex "><a href="" onClick={(e) => {e.preventDefault();op.current.toggle(e)}}><FormattedMessage id="key.license" defaultMessage="Licence BSD-3-Clause"/></a></span>
        </Fragment>
    )

}

export default Licence