import Int "mo:base/Int";
import Time "mo:base/Time";

actor {

    public query func ping () : async Int {
        Time.now()
    };

};